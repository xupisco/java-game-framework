package net.xupisco.framework;

import net.xupisco.framework.Graphics.PixmapFormat;

public interface Pixmap {
    public int getWidth();
    public int getHeight();

    public PixmapFormat getFormat();

    public void dispose();
}
